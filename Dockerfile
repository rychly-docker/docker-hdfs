ARG HADOOP_VERSION
FROM registry.gitlab.com/rychly-edu/docker/docker-hadoop-base:${HADOOP_VERSION:-latest}

MAINTAINER Marek Rychly <marek.rychly@gmail.com>

COPY scripts /

RUN true \
# make the scripts executable
&& chmod 755 /*.sh

ENTRYPOINT ["/entrypoint.sh"]

HEALTHCHECK CMD /healthcheck.sh
