#!/bin/sh

exec mkdir -vp volumes/namenode volumes/namenode-secondary $(seq -f 'volumes/datanode%g' -s ' ' 1 10)
