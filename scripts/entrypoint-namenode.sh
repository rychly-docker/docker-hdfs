#!/usr/bin/env bash

DIR=$(dirname "${0}")

. "${DIR}/hadoop-entrypoint-helpers.sh"

# from docker-hadoop-base/scripts/application-helpers.sh
. "${DIR}/application-helpers.sh"

set_dfs_default

set_name_dirs

set_dfs_namenodes

add_users

. "${DIR}/hadoop-set-props.sh"

if [ ! -e "${NAMENODE_FORMATTED_FLAG:=/hdfs-namenode.formated}" ]; then
	su hadoop -c "exec ${HADOOP_HOME}/bin/hdfs namenode -format -nonInteractive"
	touch "${NAMENODE_FORMATTED_FLAG}"
fi

# from docker-hadoop-base/scripts/application-helpers.sh
wait_for

exec su hadoop -c "exec ${HADOOP_HOME}/bin/hdfs namenode ${@}"
