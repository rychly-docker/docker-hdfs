#!/bin/sh

case "${ROLE}" in
	datanode)
		exec su hadoop -c '${HADOOP_HOME}/bin/hdfs dfsadmin -getDatanodeInfo $(${HADOOP_HOME}/bin/hdfs getconf -confKey dfs.datanode.ipc.address) | grep -q Uptime: || exit 1'
		;;
	namenode)
		exec su hadoop -c '${HADOOP_HOME}/bin/hdfs dfs -df ${DFS_DEFAULT}/ || exit 1'
		;;
	namenode-secondary)
		exec su hadoop -c 'SNNC=$(${HADOOP_HOME}/bin/hdfs getconf -secondaryNameNodes | wc -l); test "${SNNC}" -gt 0 || exit 1'
		;;
	*)
		echo "Unknow role '${ROLE}', cannot perform the health-check." >&2
		exit 0
esac
